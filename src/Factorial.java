import java.util.Scanner;

public class Factorial {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int i,j,sum,fact,k=0;
		int[] vect;
		vect=new int[20];
		int a,b;
		Scanner cin=new Scanner(System.in);
		System.out.print("Dati primul numar a= ");
		a=cin.nextInt();
		System.out.print("Dati al doilea numar b= ");
		b=cin.nextInt();

		for(i=a;i<=b;i++)
		{
			fact=1;
			for(j=1;j<=i;j++)
			{
				fact=fact*j;
			}
			System.out.println("factorial de "+i+ " este "+fact);
			sum=0;
			while(fact!=0)
			{
				sum+=fact%10;
				fact/=10;
			}
			vect[k++]=sum;
		}
		System.out.println("vectorul de sume este : ");
		for(i=0;i<k;i++)
			System.out.println(vect[i]);
		
		
		int index_inceput=0,max=0,lung=1;
		for(i=0;i<k-1;i++)
		{
			if(vect[i]>=vect[i+1])
				lung++;
			else
				lung=1;
			if(lung>=max)
			{
				max=lung;
				index_inceput=i-lung+2;
			}	
		}
		
		System.out.println("cea mai lunga secventa este:");
		for(i=0;i<max;i++)
		{
			System.out.println(vect[i+index_inceput]);
		}
		cin.close();
		
	}

}
